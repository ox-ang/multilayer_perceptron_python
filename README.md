# Multilayer perceptron code #

author: yves weissenberger
email: yvesweissenberger@gmail.com


Ipython notebooks for running multilayer perceptron. Written in numpy. 
Currently runs on MNIST dataset with added loader function.
Training occurs via minibatch stochastic gradient descent.

## File Descriptions ##
Multilayer_perceptron_p_backprop.ipynb - Parallelised stochastic gradient descent (over training examples in minibatch)

### To do list ###
* Cross Entropy Cost Function
* Non Gaussian Error Derivatives
* Improve readme.md
* Multi-neuron output